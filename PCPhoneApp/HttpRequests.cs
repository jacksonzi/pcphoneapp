﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Windows.Storage.Streams;
using Windows.Web.Http;
using Windows.Web.Http.Filters;

namespace PCPhoneApp
{
    /// <summary>
    /// This class contains the common methods to manage HTTP methods.
    /// </summary>
    public sealed class HttpRequests
    {
        private const int DEFAULT_TIMEOUT = 60000;
        /// <summary>
        /// Execute asynchronously a HTTP GET request to Uri passed by parameter.
        /// </summary>
        /// <param name="targetUri">The target Uri of the request.</param>
        /// <returns>A string with server response.</returns>
        public async Task<string> ExecuteGetRequestAsync(string targetUri, Action<Exception> exceptionHandler = null)
        {
            try
            {
                Uri builtUri = new Uri(targetUri, UriKind.Absolute);

                //Using HTTP filter to don't allow cache storage.
                HttpBaseProtocolFilter _httpFilter = new HttpBaseProtocolFilter();
                _httpFilter.CacheControl.ReadBehavior = HttpCacheReadBehavior.MostRecent;
                _httpFilter.CacheControl.WriteBehavior = HttpCacheWriteBehavior.NoCache;

                //Deleting possible cookies.
                HttpCookieManager _clientCookieManager = _httpFilter.CookieManager;
                HttpCookieCollection cookies = _clientCookieManager.GetCookies(builtUri);
                if (cookies != null && cookies.Count > 0)
                {
                    foreach (var cookie in cookies)
                    {
                        _clientCookieManager.DeleteCookie(cookie);
                    }
                }

                //Finally, we do the request.
                using (HttpClient _client = new HttpClient(_httpFilter))
                {
                    CancellationTokenSource cancellationTokenSource = new CancellationTokenSource(DEFAULT_TIMEOUT);
                    string serverResponse = await _client.GetStringAsync(builtUri).AsTask(cancellationTokenSource.Token);

                    return serverResponse;
                }
            }
            catch (Exception e)
            {
                return InvokeExceptionHandler(exceptionHandler, e);
            }
        }

        /// <summary>
        /// Execute a HTTP GET request to Uri passed by parameter and open a stream to server resource.
        /// </summary>
        /// <param name="targetUri">The target Uri of the request.</param>
        /// <returns>A stream with server response.</returns>
        public async void ExecuteGetRequestStream(string targetUri, Stream dataStream)
        {
            using (HttpClient _client = new HttpClient())
            {
                CancellationTokenSource cancellationTokenSource = new CancellationTokenSource(DEFAULT_TIMEOUT);
                Uri builtUri = new Uri(targetUri, UriKind.Absolute);
                HttpResponseMessage _serverResponse = await _client.GetAsync(builtUri).AsTask(cancellationTokenSource.Token);
                dataStream = (await _serverResponse.Content.ReadAsInputStreamAsync()).AsStreamForRead();
            }
        }

        /// <summary>
        /// Execute a HTTP POST request to Uri passed by parameter.
        /// </summary>
        /// <param name="targetUri">The target Uri of the request.</param>
        /// <param name="body">The body (payload) content of the request.</param>
        /// <returns>A string with server response.</returns>
        public async Task<string> ExecutePostRequestAsync(string targetUri, string body, Action<Exception> exceptionHandler = null)
        {
            try
            {
                Uri builtUri = new Uri(targetUri, UriKind.Absolute);

                //Using HTTP filter to don't allow cache storage.
                HttpBaseProtocolFilter _httpFilter = new HttpBaseProtocolFilter();
                _httpFilter.CacheControl.ReadBehavior = HttpCacheReadBehavior.MostRecent;
                _httpFilter.CacheControl.WriteBehavior = HttpCacheWriteBehavior.NoCache;

                //Deleting possible cookies.
                HttpCookieManager _clientCookieManager = _httpFilter.CookieManager;
                HttpCookieCollection cookies = _clientCookieManager.GetCookies(builtUri);
                if (cookies != null && cookies.Count > 0)
                {
                    foreach (var cookie in cookies)
                    {
                        _clientCookieManager.DeleteCookie(cookie);
                    }
                }

                using (HttpClient _client = new HttpClient(_httpFilter))
                {
                    CancellationTokenSource cancellationTokenSource = new CancellationTokenSource(DEFAULT_TIMEOUT);

                    HttpStringContent _stringContent = new HttpStringContent(body, Windows.Storage.Streams.UnicodeEncoding.Utf8, "application/x-www-form-urlencoded");
                    using (HttpResponseMessage _serverResponse = await _client.PostAsync(builtUri, _stringContent).AsTask(cancellationTokenSource.Token))
                    {
                        string serverResponseString = await _serverResponse.Content.ReadAsStringAsync();
                        return serverResponseString;
                    }
                }
            }
            catch (Exception e)
            {
                return InvokeExceptionHandler(exceptionHandler, e);
            }
        }

        /// <summary>
        /// This method extraxt the JSON part into a string, it can be used for remove
        /// HTML, PHP code from server response.
        /// </summary>
        /// <param name="rawContent">The full server response</param>
        /// <returns>The JSON part as string or String.Empty if there is no JSON part into rawContent</returns>
        public string FilterJson(string rawContent)
        {
            StringBuilder result = new StringBuilder(string.Empty);
            foreach (Match match in Regex.Matches(rawContent, "\\[?{.+}\\]?", RegexOptions.IgnoreCase))
            {
                result.Append(match.Value);
            }
            return result.ToString();
        }

        private string InvokeExceptionHandler(Action<Exception> exceptionHandler, Exception e)
        {

            return null;
        }
    }
}
